const mongoose = require("mongoose");

const CartSchema = new mongoose.Schema({
  
    userId: { 
      type: String, 
      required: true 
    },
    productId: {
      type: String,
    },
    name: {
      type: String,
    },
    quantity: {
      type: Number,
      default: 1,
    },
    price: {
      type: Number,
      required: true
    },
    subTotal: {
      type: Number,
      required: true
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("Cart", CartSchema);