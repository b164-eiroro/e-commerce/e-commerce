const express = require("express");
const router = express.Router();
const auth = require("../auth");
const ProductController = require("../controllers/productController");

//ADD PRODUCT
router.post("/", auth.verify, (req, res) => {
    const data = {
        products: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    if(data.isAdmin){
        ProductController.addProduct(req.body).then(result => res.send(result))
    } else {
        res.send({ auth: 'You are not authorize'})
    }
})

//GET ACTIVE
router.get("/active", (req, res) => {
    ProductController.getAllActive().then(result => res.send(result));
})


//GET ALL
router.get("/all", (req, res) => {
    ProductController.getAll().then(result => res.send(result));
})

//GET PRODUCT BY ID
router.get("/:id", (req, res) => {
    ProductController.getOne(req.params.id).then(result => res.send(result));
})


//UPDATE A PRODUCT
router.put("/update/:id", auth.verify, (req, res) => {
    const data = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
    } 
    if(data.isAdmin) {
        ProductController.updateProduct(req.params.id, req.body).then(result => res.send(result))
    } else {
        res.send(false)
    }
})

//DELETE A PRODUCT
router.delete("/delete/:id", auth.verify, (req, res) => {
    const userData = {
        isAdmin : auth.decode(req.headers.authorization).isAdmin,
    }
    if(userData.isAdmin){
       ProductController.deleteProduct(req.params.id).then(result => res.send(result));
    } else {
        res.send({auth : "You're not authorize to do this"})
    }
})

//ARCHIVE PRODUCTS
router.put("/archive/:productId", auth.verify, (req, res) => {
    const data = {
        isAdmin : auth.decode(req.headers.authorization).isAdmin
    }
    if(data.isAdmin){
        ProductController.archiveProduct(req.params.productId).then(result => res.send(result))
    } else {
        res.send(false)
    }
})

//ACTIVATE A PRODUCT
router.put("/activate/:productId", auth.verify, (req, res) => {
    const data = {
        isAdmin : auth.decode(req.headers.authorization).isAdmin
    }
    if(data.isAdmin){
        ProductController.activateProduct(req.params.productId).then(result => res.send(result))
    } else {
        res.send(false)
    }
})


module.exports = router;