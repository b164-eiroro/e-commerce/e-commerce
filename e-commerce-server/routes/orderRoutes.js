const express = require("express");
const router = express.Router();
const auth = require("../auth");
const OrderController = require("../controllers/orderController");


//create Order
router.post("/", auth.verify, (req, res) => {
    const userData = {
        userId : auth.decode(req.headers.authorization).id,
        name : req.body.name,
        amount : req.body.amount,
        quantity : req.body.quantity,
        subTotal: req.body.amount
    }
        OrderController.createOrder(userData).then(result => res.send(result))
    }
)

//Retrieved user order
router.get("/my-orders", auth.verify, (req, res) => {
    const userData = {
        userId: auth.decode(req.headers.authorization).id,
    }
    OrderController.getOne(userData).then(result => res.send(result));
})

module.exports = router;	