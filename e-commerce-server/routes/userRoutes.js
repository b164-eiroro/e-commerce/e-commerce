const express = require('express');
const router = express.Router();
const auth = require('../auth');

const UserController = require('../controllers/userController');


// REGISTER USER
router.post('/register', (req, res) => {
	UserController.registerUser(req.body).then(result => res.send(result))
});

//CHECK EMAIL
router.post("/checkEmail", (req, res) => {
	UserController.checkEmailExist(req.body).then(result => res.send(result));
});

//LOGIN
router.post("/login", (req, res) => {
    UserController.loginUser(req.body).then(result => res.send(result));
})

//USER DETAILS
router.get("/details", auth.verify, (req, res) => {
	//decode() to retrieve the user information from the token passing the "token" from te request headers as an argument
	const userData = auth.decode(req.headers.authorization);
	UserController.getDetails(userData.id).then(result => res.send(result))
} )



module.exports = router;