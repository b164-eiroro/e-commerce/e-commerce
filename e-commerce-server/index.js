const express = require("express");
const mongoose = require("mongoose");
require('dotenv').config();
const cors = require("cors");

const userRoutes = require("./routes/userRoutes");
const productRoutes = require('./routes/productRoutes');
// const cartRoutes = require('./routes/cartRoutes');

const orderRoutes = require("./routes/orderRoutes");

const app = express();


app.use(cors());
app.use(express.json());
app.use(express.urlencoded( {  extended: true } ));

app.use("/api/users", userRoutes);
app.use("/api/products", productRoutes);
// app.use("/api/cart", cartRoutes);
app.use("/api/order", orderRoutes);

// app.use("/order", orderRoutes);

mongoose.connect(process.env.DB_CONNECTION, {
    useNewUrlParser: true,
    UseUnifiedTopology: true
})


mongoose.connection.once('open', () => console.log('You are now connected to mongoDB Atlas'));

app.listen(process.env.PORT || 4000, () => {
    console.log(`E-CommerceAPI is now online on port ${process.env.PORT}`);
})