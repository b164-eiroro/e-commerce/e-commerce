const Order = require("../models/Order");


//create order
module.exports.createOrder = (reqBody) => {
    let newOrder = new Order ({
        userId : reqBody.userId,
        name : reqBody.name,
        amount : reqBody.amount,
        quantity : reqBody.quantity,
        subTotal: reqBody.amount * reqBody.quantity
    })
    return newOrder.save().then((order, error) => {
        if(error) {
            return error;
        } else {
            return order;
        }
    })
}

//Retrieve user product
module.exports.getOne = (data) => {
    return Order.find({ userId : data.userId}).then(result => {
        return result;
    })
}