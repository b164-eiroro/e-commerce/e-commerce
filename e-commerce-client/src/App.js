import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import { UserProvider } from './UserContext';
import { useState, useEffect } from 'react';

import Register from './pages/Register';
import AppNavbar from './components/AppNavbar';
import Logout from './pages/Logout';
import Cart from "./pages/Cart";
import Admin from './pages/Admin';
import ProductView from './pages/ProductView';
import SignIn from './pages/SignIn';
import Products from './pages/Products';
import ErrorPage from './pages/Error';
import Home from './pages/Home';
import Announcement from './components/Announcement';
import Order from './pages/Order';


import './App.css';

export default  function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear()
  }

  useEffect(() => {

    fetch("http://localhost:4000/api/users/details",{
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}` 
      }
    })
    .then(res => res.json())
    .then(data => {
      if(typeof data._id !== "undefined") {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      } else {
        setUser({
          id: null,
          isAdmin: null
      })
      }
    })

  }, [] )

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <Announcement/>
        <AppNavbar />
        <Routes>
          <Route path='/register' element={<Register/>}/>
          <Route path='/' element={<Home/>}/>
          <Route path='/logout' element={<Logout/>}/>
          <Route path='/signin' element={<SignIn/>}/>
          <Route path='/products' element={<Products/>}/>
          <Route path='/orders' element={<Order/>}/>
          <Route path='/products/:productId' element={<ProductView />}/>
          <Route path='/admin' element={<Admin/>}/>
          <Route path='/cart' element={<Cart/>}/>
          <Route path='*' element={<ErrorPage/>}/>
        </Routes>
      </Router>
    </UserProvider>
  );
};


