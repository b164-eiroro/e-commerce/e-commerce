import styled from "styled-components";

const Container = styled.div`
  height: 30px;
  background-color: #00A7E1;
  color: white;
  display: flex;
  justify-content: center;
  font-size: 14px;
  font-weight: 500;
  padding-top: 5px;
`;

const Announcement = () => {
  return <Container>  BEST CLOTHING DESIGNS IN THE SOUTH!</Container>;
};

export default Announcement;
