import { Container, Col, Card } from "react-bootstrap"

export default function OrderCard({orderProp}) {
  const {userId, name, quantity, amount, subTotal } = orderProp

  return (
    <Container className='m-3'>
      <Col>
        <Card>
          <Card.Subtitle>User:{userId}</Card.Subtitle>
          <Card.Subtitle>Product:{name}</Card.Subtitle>
          <Card.Subtitle>Quantity: {quantity}</Card.Subtitle>
          <Card.Subtitle>amount: {amount}</Card.Subtitle>
          <Card.Subtitle>Subtotal: {subTotal}</Card.Subtitle>
        </Card>
      </Col>
    </Container>
  )
}
  
